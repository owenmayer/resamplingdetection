GUI.load_but.Callback = 'gui_load(GUI)';
GUI.load_brws.Callback = 'gui_browse(GUI)';
GUI.fft_replot.Callback = 'gui_plot_fft(GUI)';

if isfield(GUI,'im_trans'); %demo version?
    GUI.im_trans.Callback = 'gui_transform(GUI)';
    GUI.im_res.Callback = 'gui_trans_reset(GUI)';
else %tool version
    GUI.im_poly.Callback = 'gui_imdrawpoly(GUI)';
    GUI.im_free.Callback = 'gui_imfreehandroi(GUI)';
    GUI.im_crop.Callback = 'gui_imcroproi(GUI)';
end
