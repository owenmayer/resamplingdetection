function gui_trans_reset(GUI)

I = imread(GUI.load_str.String);
axes(GUI.ax_im);
imshow(I);
GUI.ax_im.UserData = I;

GUI.im_rot.String = 0;
GUI.im_scl.String = 1;
GUI.im_intrp.Value = 1;

gui_plot_pmap(GUI);
%update size annotation
gui_imsize_text(GUI);
gui_plot_fft(GUI);