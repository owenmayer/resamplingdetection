%gui_plot_fft
function gui_plot_fft(GUI)

nfft = str2double(GUI.fft_nfft.String);
rLF = str2double(GUI.fft_lfr.String);
isRmvJPEG = GUI.fft_rmvJpg.Value;
rJPEG = str2double(GUI.fft_radJpg.String);



pAC = GUI.ax_pmap.UserData; %get pmap


pPad = zeros(nfft,nfft); %intialize padded pmap
yfill = min(size(pPad,1),size(pAC,1)); %whether to overpad or underpad
xfill = min(size(pPad,2),size(pAC,2));

pPad(1:yfill,1:xfill) = pAC(1:yfill,1:xfill); %pull in pmap data

%find fft midpoints
ymid = nfft/2 +1;
xmid = nfft/2 +1;

Y = fft2(pPad);
Y = fftshift(fftshift(Y,2),1);

Y(circleIndices(nfft,nfft,xmid,ymid,rLF)) = eps;

if isRmvJPEG
    Y = rsdetect_removejpegfft(Y,rJPEG);
end

%plot that ish
axes(GUI.ax_fft);
imagesc(abs(Y).^2); colormap(gray); axis equal;
axis tight; axis off

%save data for later
GUI.ax_pmap.UserData = pAC;
