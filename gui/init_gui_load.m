%% PARAMETERS
%load panel
ploadW = 1; ploadH = 0.1;
load_brwsS = 0.05; load_brwsW = 0.05; load_brwsH = 0.5; 
load_strS = load_brwsS+load_brwsW+0.025; 
load_strW = 0.25; load_strH = 0.4;
load_butS = load_strS+load_strW+0.025;
load_butW = 0.05; load_butH = 0.5;

%% LOAD PANEL
ploadim = uipanel(hfig);
ploadim.Position = [0 1-ploadH ploadW ploadH];
ploadim.Title = 'Load Image';

%browse button
load_brws = uicontrol('Style','Pushbutton','Parent',ploadim,...
    'Units','normalized');
load_brws.Position = [load_brwsS 0.5-load_brwsH/2 load_brwsW load_brwsH];
load_brws.String = 'Browse';

%image name field
load_str = uicontrol('Style','edit','Parent',ploadim,...
    'Units','normalized');
load_str.Position = [load_strS 0.5-load_strH/2 load_strW load_strH];

%image load field
load_but = uicontrol('Style','Pushbutton','Parent',ploadim,...
    'Units','normalized');
load_but.Position = [load_butS 0.5-load_butH/2 load_butW load_butH];
load_but.String = 'Load';
load_but.FontWeight = 'Bold';
