function gui_load(GUI)

%get file name
fname = GUI.load_str.String;

%load in file
I = imread(fname);

%display image
axes(GUI.ax_im);
imshow(I);

%save image data to gui struct
GUI.ax_im.UserData = I;

%plot pmap for uncompressed image
gui_plot_pmap(GUI);
%update size annotation
gui_imsize_text(GUI);
%plot_fft
GUI.fft_nfft.String = num2str(2^nextpow2(max(size(I))));
gui_plot_fft(GUI);
