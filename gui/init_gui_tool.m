function GUI = init_gui_tool(hfig)
%% PANELS

%LOAD PANEL
init_gui_load;

%IMAGE PANEL
init_gui_imagedisp_tool;

%ANALYSIS PANEL
init_gui_analysis

%% AXES
ax_im = axes('Parent',pim);
ax_pmap = axes('Parent',ppmap);
ax_fft = axes('Parent',pfft);

%% image size text
im_siz = uicontrol('Style','text','parent',pim,...
    'units','normalized');
im_sizH = 0.1;
im_siz.Position = [ax_im.Position(1), ax_im.Position(2)-im_sizH-.01,...
    ax_im.Position(3), im_sizH];
im_siz.String = 'Image size:';

%% CREATE STRUCTURE
GUI = struct('ax_im',ax_im,'ax_fft',ax_fft,'ax_pmap',ax_pmap,...
    'im_poly',im_poly,'im_res',im_res,'im_crop',im_crop,...
    'im_free',im_free,'load_brws',load_brws,'load_str',load_str,...
    'load_but',load_but,'panlys',panlys,'pfft',pfft,'pload',ploadim,...
    'ppmap',ppmap,'im_siz',im_siz,'fft_lfr',fft_lfr,'fft_nfft',fft_nfft,...
    'fft_radJpg',fft_radJpg,'fft_replot',fft_replot,'fft_rmvJpg',fft_rmvJpg);