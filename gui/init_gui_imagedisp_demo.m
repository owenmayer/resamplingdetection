%image transform panel
im_sclX = 0.125; im_sclW = 0.1; 

im_rotW = im_sclW; 
im_rotX = im_sclX+im_sclW+im_rotW/2;

im_intrpW = 0.2;
im_intrpX = im_rotX + im_rotW + 0.03;

im_transW = 0.1;
im_transX = im_intrpX + im_intrpW + 0.03;

im_resW = 0.08;
im_resX = im_transX + im_transW + 0.04;

im_editY = 0.94; im_editH = 0.033;

im_textY = im_editY + im_editH + 0.001;
im_textH = 0.02;

%% IMAGE PANEL
pim = uipanel(hfig);
pim.Position = [0 0 0.5 1-ploadH];
pim.Title = 'Image';

%scale
im_scl = uicontrol('Style','edit','parent',pim,...
    'Units','normalized');
im_scl.Position = [im_sclX im_editY im_sclW im_editH];
im_scl.String = 1;

%rotation
im_rot = uicontrol('Style','edit','parent',pim,...
    'Units','normalized');
im_rot.Position = [im_rotX im_editY im_rotW im_editH];
im_rot.String = 0;

%interpolation
im_intrp = uicontrol('Style','popupmenu','parent',pim,...
    'Units','normalized');
im_intrp.String = {'bilinear';'bicubic';'nearest'};
im_intrp.Position = [im_intrpX im_editY im_intrpW im_editH];

%transform
im_trans = uicontrol('Style','pushbutton','parent',pim,...
    'Units','normalized');
im_trans.Position = [im_transX im_editY im_transW im_editH];
im_trans.String = 'Transform';
im_trans.FontWeight = 'Bold';

%reset
im_res = uicontrol('Style','pushbutton','parent',pim,...
     'Units','normalized');
im_res.Position = [im_resX im_editY im_resW im_editH];
im_res.String = 'Reset';

%labels
uicontrol('Style','text','parent',pim,... %scale text
    'Units','normalized','String','Scale',...
    'Position',[im_sclX im_textY im_sclW im_textH]);
uicontrol('Style','text','parent',pim,... %rotate text
    'Units','normalized','String','Rot. (deg)',...
    'Position',[im_rotX im_textY im_rotW im_textH]);
uicontrol('Style','text','parent',pim,... %interpolation text
    'Units','normalized','String','Interpolation',...
    'Position',[im_intrpX im_textY im_intrpW im_textH]);


%display image size
