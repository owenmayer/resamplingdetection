function gui_imfreehandroi(GUI)

if ~isempty(GUI.im_crop.UserData) %is there already a roi defined?
    delete(GUI.im_crop.UserData); %if so, annhilate that ish
end

%focus on image axes
axes(GUI.ax_im);

%draw polygon
h = imfreehand; 

%save the polygon handle to the crop uicontrol
GUI.im_crop.UserData = h;