function gui_imsize_text(GUI)

SZ = size(GUI.ax_im.UserData);
GUI.im_siz.String = ['Image size: ' num2str(SZ(2)) ' x '...
    num2str(SZ(1))];