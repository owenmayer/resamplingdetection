%% PARAMETERS
%fft panel
fft_editY = 0.94; fft_editH = 0.033;

fft_nfftX = 0.125; fft_nfftW = 0.1;
fft_lfrX = fft_nfftX + fft_nfftW + 0.04; fft_lfrW = 0.1;
fft_rmvJpgX = fft_lfrX + fft_lfrW + 0.04; fft_rmvJpgW = 0.1;
fft_radJpgX = fft_rmvJpgX + fft_rmvJpgW + 0.04; fft_radJpgW = 0.1;
fft_replotX = fft_radJpgX + fft_radJpgW + 0.066; fft_replotW = 0.1;


fft_textY = fft_editY + fft_editH + 0.001;
fft_textH = 0.02;

%% ANALYSIS PANEL
panlys = uitabgroup(hfig);
panlys.Position = [0.5 0 0.5 1-ploadH];


%% PMAP panel
%pmap tab
ppmap = uitab(panlys);
ppmap.Title = 'P-Map';

%tau,lambda, sig, a

%% FFT Panel
pfft = uitab(panlys);
pfft.Title = 'FFT';

%nfft
fft_nfft = uicontrol('Style','edit','parent',pfft,...
    'Units','normalized');
fft_nfft.Position = [fft_nfftX fft_editY fft_nfftW fft_editH];
fft_nfft.String = 512;

%LF radius
fft_lfr = uicontrol('Style','edit','parent',pfft,...
    'Units','normalized');
fft_lfr.Position = [fft_lfrX fft_editY fft_lfrW fft_editH];
fft_lfr.String = 10;

%Remove JPEG?
fft_rmvJpg = uicontrol('Style','checkbox','parent',pfft,...
    'Units','normalized');
fft_rmvJpg.Position = [fft_rmvJpgX fft_editY fft_rmvJpgW fft_editH];
fft_rmvJpg.Value = false;
fft_rmvJpg.String = '<html>Remove<br>JPEG?';

%JPEG radius
fft_radJpg = uicontrol('Style','edit','parent',pfft,...
    'Units','normalized');
fft_radJpg.Position = [fft_radJpgX fft_editY fft_radJpgW fft_editH];
fft_radJpg.String = 1;

%JPEG radius
fft_replot = uicontrol('Style','pushbutton','parent',pfft,...
    'Units','normalized');
fft_replot.Position = [fft_replotX fft_editY fft_replotW fft_editH];
fft_replot.String = 'Replot';

%Labels
uicontrol('Style','text','parent',pfft,... %scale text
    'Units','normalized','String','NFFT',...
    'Position',[fft_nfftX fft_textY fft_nfftW fft_textH]);
uicontrol('Style','text','parent',pfft,... %rotate text
    'Units','normalized','String','L.F. Radius',...
    'Position',[fft_lfrX fft_textY fft_lfrW fft_textH]);
uicontrol('Style','text','parent',pfft,... %interpolation text
    'Units','normalized','String','JPEG Radius',...
    'Position',[fft_radJpgX fft_textY fft_radJpgW fft_textH]);