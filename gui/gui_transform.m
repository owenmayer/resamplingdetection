function gui_transform(GUI)

scl = str2double(GUI.im_scl.String); %scale value
rot = pi*str2double(GUI.im_rot.String)/180; %rotate value
intrp_type = GUI.im_intrp.String{GUI.im_intrp.Value}; %interpolation type

SCL = [scl 0 0; 0 scl 0; 0 0 1];
ROT = [cos(rot) sin(rot) 0; -sin(rot) cos(rot) 0; 0 0 1];
A =  SCL*ROT; %scale, then rotate... can change order later.. or make selectable
T = affine2d(A); %create transform

I = imread(GUI.load_str.String);
I2 = imwarp(I,T,intrp_type); 

axes(GUI.ax_im);
imshow(I2);

GUI.ax_im.UserData = I2;

gui_plot_pmap(GUI);
%update size annotation
gui_imsize_text(GUI);
gui_plot_fft(GUI);