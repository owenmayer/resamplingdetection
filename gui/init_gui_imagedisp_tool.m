%image transform panel
im_polyX = 0.15; im_polyW = 0.1; 

im_freeW = 0.1; 
im_freeX = im_polyX+im_polyW+0.03;

im_cropW = 0.1;
im_cropX = im_freeX + im_freeW + 0.03;

im_resW = 0.1;
im_resX = 1-im_resW-0.125;

im_editY = 0.94; im_editH = 0.033;

im_textY = im_editY + im_editH + 0.001;
im_textH = 0.02;

%% IMAGE PANEL
pim = uipanel(hfig);
pim.Position = [0 0 0.5 1-ploadH];
pim.Title = 'Image';

%scale
im_poly = uicontrol('Style','pushbutton','parent',pim,...
    'Units','normalized');
im_poly.Position = [im_polyX im_editY im_polyW im_editH];
im_poly.String = 'Polygon';

%rotation
im_free = uicontrol('Style','pushbutton','parent',pim,...
    'Units','normalized');
im_free.Position = [im_freeX im_editY im_freeW im_editH];
im_free.String = 'Freehand';

%interpolation
im_crop = uicontrol('Style','pushbutton','parent',pim,...
    'Units','normalized');
im_crop.String = '<html><b>Crop ROI</b>';
im_crop.Position = [im_cropX im_editY im_cropW im_editH];

%reset
im_res = uicontrol('Style','pushbutton','parent',pim,...
     'Units','normalized');
im_res.Position = [im_resX im_editY im_resW im_editH];
im_res.String = 'Reset';

