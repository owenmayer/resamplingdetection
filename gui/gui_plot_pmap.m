function gui_plot_pmap(GUI)

a = [-0.25 0.5 -0.25;
    0.5   0   0.5;
    -0.25 0.5 -0.25];

axes(GUI.ax_pmap);
[P,pAC] = rsdetect_pmap_kirchner(rgb2gray(GUI.ax_im.UserData),a,1,1,2,'same');

if ~isempty(GUI.im_crop.UserData) %cropped image?
    h = GUI.im_crop.UserData; %roi
    mask = createMask(h); %create mask from roi
    [yy,xx] = find(mask); %find indices (for dimensions later)
    mask2 = mask(min(yy):max(yy),min(xx):max(xx)); %mask for cropped area
    pAC(~mask2) = 0; %set non-image pmap to 0 (instead of 1)
    P(~mask2) = 0;
end

imagesc(P); colormap(gray); axis equal;
axis tight; axis off

GUI.ax_pmap.UserData = P;