%gui browse for image
function gui_browse(GUI)

fstr = imgetfile;

[fdir,fname,fext] = fileparts(fstr);

if strcmp(cd,fdir) %is file in current directory?
    GUI.load_str.String = horzcat(fname,fext); %just display name
else %not in directory?
    GUI.load_str.String = fstr; %specify full path
end

gui_load(GUI); %load that ish