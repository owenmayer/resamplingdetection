function gui_imcroproi(GUI)
h = GUI.im_crop.UserData; %get ROI handle

if isempty(h) %no roi?
    return; %then get outta here!
end

I = imread(GUI.load_str.String); %load image
%... should probably actually save the image somewhere instead of loading
%it everytime... problem for another day

mask = createMask(h); %create mask from roi
[yy,xx] = find(mask); %find indices (for dimensions later)
mask = repmat(mask,1,1,3);

Itemp = uint8(mask).*I;
Iout = Itemp(min(yy):max(yy),min(xx):max(xx),:);

GUI.ax_im.UserData = Iout;

gui_plot_pmap(GUI);
%update size annotation
gui_imsize_text(GUI);
gui_plot_fft(GUI)