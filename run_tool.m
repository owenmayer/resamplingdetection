close all; clearvars;
%create figure window
addpath('src','gui');

hfig = figure('units','normalized','Position',[0 0 1 1],...
    'Name','Resampling Detection Tool - MISL @ Drexel',...
    'NumberTitle','off',...
    'Toolbar','figure','MenuBar','none');

GUI = init_gui_tool(hfig);
init_callbacks;

GUI.load_str.String = 'mandril_color.tif';
gui_load(GUI);
