%removejpegfft

%function to remove jpeg related artifacts from an image 2D fft

%Owen Mayer, owen@drexel.edu, 6 Nov 2015
%MISL, Drexel University 

%INPUTS:
%Y - Image FFT, shifted so center is in middle
%r - radius of freq bins to remove (2 is good?)

%OUTPUTS
%Y - Image FFT with jpeg artifacts removed
%jc - fft mask with 1 at jpeg locations and 0 elsewhere

%JPEG compression introduces periodicity into resampling pmap at 8-pixel
%periods. For rescaling/rotation/transformation analysis it is often
%beneficial to remove these JPEG related artifacts. This script sets the
%FFT at JPEG locations to 0

function [Y, jc] = rsdetect_removejpegfft(Y,r)

%find fft midpoints
ymid = size(Y,1)/2 +1;
xmid = size(Y,2)/2 +1;

%find jpeg freq index (assumes 8x8 jpeg blocking)
ky = size(Y,1)/8; %in y
kx = size(Y,2)/8; %in x

jy = [fliplr(ymid:-ky:1) ymid+ky:ky:size(Y,1)]; %jpeg artifact locations in y
jx = [fliplr(xmid:-kx:1) xmid+kx:kx:size(Y,2)]; %jpeg artifact locations in x

% % jy = (ky:ky:size(Y,2)-ky) + 1; %jpeg artifact locations in y
% % jx = (kx:kx:size(Y,2)-kx) + 1; %jpeg artifact locations in x

jc = false(size(Y,1),size(Y,2)); %initialize jpeg mask
for ii = 1:8;
    for ji = 1:8; %iterate through each jpeg index
        jc = jc | circleIndices(size(Y,2),size(Y,1),jx(ii),jy(ji),r); %zeros
%         jc = jc | circleIndices(size(Y,2),size(Y,1),jx(ji),ymid,r); %zero in x
    end
end

Y(jc) = eps; %set to really small. but not zero incase we want to take the log later