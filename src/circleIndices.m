%circular indices

% ydim = 200;
% xdim = 100;
% 
% xc = 0; yc = 20;
% r = 10;

%INPUTS
%m - x dimension
%n - y dimension

%OUTPUTS
%xc - center in x
%yc - center in y
%r - radius

function C = circleIndices(m,n,xc,yc,r)

%initialize matrix with zeros
C = false(n,m); 

%meshify dimensions
[xx,yy] = meshgrid(1:m,1:n);

%find distance of each pt to center
D = sqrt((xx-xc).^2 + (yy-yc).^2);

%set elements in circle to true;
C(D <= r) = true;