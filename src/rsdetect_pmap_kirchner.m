%RSDETECT_GETPMAP_KIRCHNER

%Function to generate the p-map for image re-sampling detection

%Algorithm attributed to Matthias Kircher from:
%"Fast and Reliable Resampling Detection by Spectral Analysis of
%Fixed Linear Predictor Residue" (MM&Sec 2008)

%Values of p represents the probability that a particular pixel is a
%linear combination of its neighbors. In a resmapled image, the p-map will
%be highly periodic. For addl info see Popescu and Farid "Exposing Digital
%Forgeries by Detecting Traces of Resampling" (IEEE Trans. Sig. Proc. 2005)

%INPUTS:
%I - M x N x D - Image data. A 3D input will generate a p-map for each
%channel separately.

%a - q x q - 2D linear predictor, double. The choice of a is not necessarily
%important other than that it is a linear predictor with center of a = 0.
%See Kirchner 2008. They suggest
%a = [-0.25 0.5 -0.25;
%       0.5   0   0.5;
%     -0.25 0.5 -0.25]

%lambda - 1 x 1 - double - p distribution controlling parameter.
%must be greater than 0

%tau - 1 x 1 - double - p distribution controlling parameter. must be
%greater than or equal to 1

%sig - 1 x 1 - double - p distribution controlling parameter. must be
%greater than 0

%type - string - either 'valid' or 'same'. this specifies domain for the 2d
%convolution


%Kirchner used lambda = 1, sig = 1, tau = 2 in his experiments

%OUTPUTS:
%p - m x n x D - p map. probability that pixel is a linear prediction
%of its neighbors

%pAC - m x n x D - p map with average (DC component) removed.

%Function by: Owen Mayer - MISL - 15 Oct 2015 - om82@drexel.edu

function [p, pAC] = rsdetect_pmap_kirchner(I,a,lambda,tau,sig,type)

%check that image is a double (required by matlab convolution)
if ~isa(I,'double');
    I = double(I);
end

switch type
   %select convolution domain (effects dimensions of output)
    case 'same'
        %Calculate linear predictor residue.
        %Eq. 5 from Kirchner 2008
        E = convn(I,a,'same') - I;
        
    case 'valid'
        %find k dimension of linear predictor
        k = (size(a,1)-1)/2;
        
        %determine the image for which the prediction is valid
        Ivalid = I(1+k:end-k,1+k:end-k,:);
        
        %Calculate linear predictor residue.
        %Eq. 5 from Kirchner 2008
        E = convn(I,a,'valid') - Ivalid;
        
    otherwise
        error(['RSDETECT_PMAP_KIRCHNER: input type "'...
            num2str(type) '" not recognized'])
end

%calculate p-map.
%Eq. 21 from Kirchner 2008
p = lambda * exp(-1*(abs(E).^tau)/sig);

%remove DC component by channel
pAC = p - repmat(mean(mean(p)),size(p,1),size(p,2));