close all; clear all; %#ok<*CLSCR>
%create figure window
hfig = figure('units','normalized','Position',[0 0 1 1],...
    'Name','Resampling Detection Demo - MISL @ Drexel',...
    'NumberTitle','off',...
    'Toolbar','figure','MenuBar','none');

GUI = init_gui_demo(hfig);
init_callbacks;

GUI.load_str.String = 'mandril_color.tif';
gui_load(GUI);
